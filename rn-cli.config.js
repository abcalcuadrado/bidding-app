/**
 * Created by xavi on 3/5/16.
 */
var blacklist= require("react-native/packager/blacklist");
var config = {
    getBlacklistRE(platform) {
        return blacklist(platform,[/bidding_app.+\/node_modules\/fbjs.*/]);
    }
}
module.exports = config;

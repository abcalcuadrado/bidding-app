var AppDispatcher = require('../dispatchers/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var actionTypes = require('../constants/actionTypes');
var assign = require('object-assign');
import React, {AsyncStorage} from 'react-native';

var CHANGE_EVENT = 'USER_CHANGE';
var user = {};

function _getCredentials() {

}

function _isLoggedUser() {
    return true;
}
async function _syncStorage() {
    return await AsyncStorage.getItem("user");
}

function _updateCredentials(credentials) {
    user.credentials = credentials;
    AsyncStorage.setItem("user", user);
}

var UserStore = assign({}, EventEmitter.prototype, {

    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    async syncStorage(){
        user = {..._syncStorage().done()};
    },
    updateCredentials(credentials){
        _updateCredentials(credentials);
    },
    isLoggedUser(){
        _isLoggedUser();
    }

});


AppDispatcher.register(function (action) {
    var text;

    switch (action.actionType) {
        case actionTypes.LOGGED_IN:
            UserStore.updateCredentials(action.payload)
            UserStore.emitChange();
            break;
        default:
    }
});

export default UserStore;

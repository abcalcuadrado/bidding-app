/**
 * Created by jorge on 30/01/16.
 */
export const EVENT = 'EVENT';
export const HOME = 'HOME';
export const CHAT = 'CHAT';
export const ACCOUNT = 'ACCOUNT';
export const SEARCH = 'SEARCH';
export const LOGIN = 'LOGIN';

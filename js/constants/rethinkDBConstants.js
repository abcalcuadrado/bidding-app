/**
 * Created by jorge on 30/01/16.
 */
export const HOST = '192.168.99.100';
export const PORT = '28015';
export const DB = 'events_app_db';
export const USERS_TABLE = 'users';
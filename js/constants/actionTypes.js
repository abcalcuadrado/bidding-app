export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const GO_TO_SEARCH = 'GO_TO_SEARCH';
export const LOGIN = 'LOGIN';
export const LOGGED_IN = 'LOGGED_IN';
export const ADD_TO_LIST = 'ADD_TO_LIST';
export const UPDATE_USER_DATA = 'UPDATE_USER_DATA';
export const RECEIVE_USER = 'RECEIVE_USER';
export const CHECK_ACTIVE_SESSION = 'CHECK_ACTIVE_SESSION';
export const GO_HOME = 'GO_HOME';
export const GO_LOGIN = 'GO_LOGIN';
export const INIT = 'redux-localstorage/INIT';
export const GO_SPLASH = 'GO_SPLASH';
export const LOGOUT = 'LOGOUT';

/**
 * Created by xavier on 2/1/16.
 */

import * as rethinkDBConstants from '../constants/rethinkDBConstants';
var Rx = require('rx');
var r = require('rethinkdb');
global.Buffer = global.Buffer || require('buffer').Buffer;
global.net = global.net || require('react-native-tcp');

var connect = Rx.Observable.fromNodeCallback(r.connect);
var connectObservable = connect({host: rethinkDBConstants.HOST, port: rethinkDBConstants.PORT});

var subscribe = function (onNext, observableFunction, observableFunctionArgs, subscriptionCallback) {
    connectObservable.subscribe(
        function (connection) {
            var observable = observableFunction(connection, ...observableFunctionArgs);
            var observer = Rx.Observer.create(
                onNext,
                onError,
                onComplete
            );
            var subscription = observable.subscribe(
                observer
            );
            if (subscriptionCallback !== undefined && typeof subscriptionCallback === "function") {
                subscriptionCallback(subscription);
            }


        },
        function (err) {
            console.log(err);

        }
    );
}
var onError = function(){
    console.log("hubo un error en el observable")
}

var onComplete = function(){
    console.log("se completo el observable")
}


module.exports.connectObservable = connectObservable;
module.exports.subscribe = subscribe;

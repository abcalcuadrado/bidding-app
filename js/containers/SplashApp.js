/**
 * Created by jorge on 26/01/16.
 */
import React, {Component, PropTypes, TouchableOpacity, View, Text, AsyncStorage} from 'react-native';
import UserStore from '../stores/UserStore';
import * as bootstrap from '../utils/bootstrap';
import {Actions} from 'react-native-router-flux';
import * as actionTypes from '../constants/actionTypes'

export default class SplashApp extends Component {
    constructor(props) {
        super(props);

    }
    componentDidMount() {
        bootstrap.init().done();
        console.log("paso algo")
        console.log(UserStore.isLoggedUser())
        setTimeout(function () {
            if (UserStore.isLoggedUser()){
                console.log(UserStore.isLoggedUser())
                Actions[actionTypes.GO_HOME]();
            }else{
                console.log(Actions[actionTypes.GO_LOGIN])
                Actions[actionTypes.GO_LOGIN]();
            }
        }, 1000)

        

    }

    render() {
        return (
            <Text>
                Splash Screen
            </Text>

        );
    }
}

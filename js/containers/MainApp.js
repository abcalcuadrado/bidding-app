import React, {Component} from 'react-native';
import * as actionTypes from '../constants/actionTypes'
import LoginApp from '../containers/LoginApp';
import SplashApp from '../containers/SplashApp';
import HomeApp from '../containers/HomeApp';
import {Router, Route, Schema, Animations, TabBar} from 'react-native-router-flux'


export default class MainApp extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

                <Router>
                    <Route name={actionTypes.GO_SPLASH} component={SplashApp} initial={true}
                           hideNavBar={true}
                           title="Splash"/>
                    <Route name={actionTypes.GO_LOGIN} component={LoginApp} hideNavBar={true} title="Login"/>
                    <Route name={actionTypes.GO_HOME} component={HomeApp} title="Home" hideNavBar={true}/>
                </Router>

        );
    }
}

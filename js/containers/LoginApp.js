import  React, {Component} from 'react-native';
import Login from '../components/Login'
import {FBLoginManager} from 'NativeModules';
import LoginActions from '../actions/LoginActions'


export default class LoginApp extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {
        console.log("estamos en el login")
    }

    onLogin() {

        return ()=>FBLoginManager.login(LoginActions.facebookLogin);
    }

    render() {


        return (
            <Login onLogin={this.onLogin()}/>


        );
    }
}

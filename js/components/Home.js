/**
 * Created by jorge on 26/01/16.
 */
import React, {Component, PropTypes, Text, View} from 'react-native';
import * as loginActions from '../actions/LoginActions';
import Login from '../components/Login';
import {FBLoginManager} from 'NativeModules';
import {Actions} from 'react-native-router-flux';


class HomeApp extends Component {

    constructor(props) {
        super(props)
    }

    onLogout() {

        var {logout} = this.props;
        FBLoginManager.logout(function (error, data) {
            logout();
        });
    }

    render() {

        return (
            <View style={{flex:1}}>
                <Text>Home</Text>
                <Login onLogin={this.onLogout.bind(this)} isLogin={false}/>
            </View>
        );
    }
}

export default connect(state => {
        return {user: state.user}
    },
    (dispatch) => bindActionCreators(loginActions, dispatch)
)(HomeApp);

import React, {
    Component, Text, View, TouchableHighlight,StyleSheet
} from 'react-native';
import {
  MKButton,
  MKColor,
} from 'react-native-material-kit';



export default class Login extends Component {
    constructor(props) {
        super(props);
    }

    render() {

      var {onLogin} = this.props;
      const ColoredRaisedButton = MKButton.coloredButton()
        .withBackgroundColor("0D47A1")
        .withText('ENTRA CON FACEBOOK')
        .withOnPress(onLogin)
        .withTextStyle(styles.loginButton)
        .build();
      return (
          <View style={styles.container}>
            <View style={styles.headSection}>
              <Text style={styles.logo}>Eventify</Text>
              <Text style={styles.slogan}>Crea eventos con tus amigos</Text>
            </View>

            <View style={styles.loginSection}>
              <ColoredRaisedButton />
              <View style={styles.loginMessageContainer}>
                <Text style={styles.loginMessageIcon}>info</Text>
                <Text style={styles.loginMessageText}>No posteamos nada en Facebook</Text>
              </View>
            </View>


          </View>

        );
    }
}

var styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection: "column",
    justifyContent:  'space-between'
  },
  headSection: {
    marginTop: 80,
    flexDirection: 'column',
    alignItems: 'center'
  },
  logo: {
    // fontFamily: 'Roboto-Thin',
    fontSize:50,
    color: '#03A9F4'
  },
  slogan: {
    // fontFamily: 'Roboto-Thin',
    fontSize:25,
    color: '#434343',
    marginTop: 5
  },
  loginSection: {
    marginLeft: 50,
    marginRight: 50,
    bottom:60,
  },
  loginMessageContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 8,

  },
  loginMessageText: {
    // fontFamily: 'Roboto-Light',
    fontSize:14,
    color: '#434343'
  },
  loginMessageIcon: {
    marginTop: 2,
    fontSize:16,
    // fontFamily: 'Material Icons',
    marginRight: 3,
    color: '#434343'
  },
  loginButton:{
    // fontFamily: 'Roboto-Regular',
    color: "#fff",
    fontSize:15
  }
});

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {AppRegistry} from 'react-native';
import MainApp from './js/containers/MainApp'
AppRegistry.registerComponent('bidding_app', () => MainApp);